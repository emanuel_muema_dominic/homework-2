import aims
from nose.tools import assert_almost_equal

def test_ints():
    numbers = [1, 2, 3, 4]
    obs = aims.std(numbers)
    exp = 1.11803398874989
    assert_almost_equal(obs, exp)

def test_negatives():
    numbers = [-1, -2, -3]
    obs = aims.std(numbers)
    exp = 0.816496580927726
    assert_almost_equal(obs, exp)

def test_floats():
    numbers = [1.2, 2.5, 3.7]
    obs = aims.std(numbers)
    exp = 1.02089285540757
    assert_almost_equal(obs, exp)

def test_mix():
    numbers = [-1.2, 2.5, 7]
    obs = aims.std(numbers)
    exp = 3.352942323127885
    assert_almost_equal(obs, exp)

def test_avg():
    filename_as_string =['data/bert/audioresult-00239','data/bert/audioresult-00235','data/bert/audioresult-00223']
    obs = aims.avg_range(filename_as_string)
    exp = 6.33333333333
    assert_almost_equal(obs, exp)

def test_onefile():
    filename_as_string =['data/bert/audioresult-00239']
    obs = aims.avg_range(filename_as_string)
    exp = 7
    assert_almost_equal(obs, exp)

def test_difffile():
    filename_as_string =['data/alexander/data_216.DATA']
    obs = aims.avg_range(filename_as_string)
    exp = 3.0
    assert_almost_equal(obs, exp)

