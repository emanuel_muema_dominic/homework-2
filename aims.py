import numpy as np

def std(numbers):
    N = len(numbers)
    X_bar = sum(numbers)/float(N)   
    ss = 0
    for i in range(N):
        ss += pow((numbers[i]-X_bar),2)
    sd = (ss/float(N))**0.5
    return sd

def avg_range(filename_as_string):
    range_value_lst =[]
    for file in filename_as_string:
        my_data_file = open(file)
        for line in my_data_file:
            if (line.startswith('Range')):
                range_list = line.strip().split(':')
                range_value_lst.append(float(range_list[1]))
        my_data_file.close()
    return (np.average(range_value_lst))
